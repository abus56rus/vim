set nocompatible " be iMproved, required
filetype off     " required

"=====================================================
" vundle settings
"=====================================================
" set the runtime path to include vundle and initialize
set rtp+=~/.vim/bundle/vundle.vim
call vundle#begin()

Plugin 'gmarik/vundle.vim'              " let vundle manage vundle, required
"---------=== code/project navigation ===-------------
Plugin 'jlanzarotta/bufexplorer'              " class/module browser
Plugin 'scrooloose/nerdtree'            " a tree explorer plugin for vim
" Plugin 'shougo/unite.vim'               " navigation between buffers and files
Plugin 'majutsushi/tagbar'              " class/module browser

"------------------=== other ===----------------------
Plugin 'bling/vim-airline'              " lean & mean status/tabline for vim that's light as air
Plugin 'fisadev/fixedtasklist.vim'      " pending tasks list
Plugin 'rosenfeld/conque-term'          " consoles as buffers
Plugin 'tpope/vim-surround'             " parentheses, brackets, quotes, xml tags, and more
" Plugin 'equalsraf/neovim-gui-shim'
"--------------=== snippets support ===---------------
Plugin 'garbas/vim-snipmate'            " snippets manager
Plugin 'marcweber/vim-addon-mw-utils'   " dependencies #1
Plugin 'tomtom/tlib_vim'                " dependencies #2
Plugin 'honza/vim-snippets'             " snippets repo
Plugin 'git://github.com/altercation/vim-colors-solarized.git'
"---------------=== languages support ===-------------
Plugin 'scrooloose/syntastic'           " syntax checking plugin for vim
Plugin 'tpope/vim-commentary'           " comment stuff out
Plugin 'mitsuhiko/vim-sparkup'          " sparkup (xml/jinja/htlm-django/etc.) support

" --- clojure ---
Plugin 'tpope/vim-fireplace'            " clojure completion
Plugin 'guns/vim-clojure-highlight'     " highlighting code
Plugin 'guns/vim-clojure-static'        " highlighting for static types

" --- erlang ---
Plugin 'jimenezrick/vimerl'             " the erlang plugin for vim

" --- css ---
Plugin 'juleswang/css.vim'              " css syntax file
Plugin 'groenewege/vim-less'            " vim syntax for less (dynamic css)

" --- javascript ---
Plugin 'pangloss/vim-javascript'        " vastly improved javascript indentation and syntax support in vim

" --- html ---
Plugin 'othree/html5.vim'               " html5 omnicomplete and sytnax
Plugin 'idanarye/breeze.vim'            " html navigation like vim-easymotion, tag matching, tag highlighting and dom navigation

" --- python ---
Plugin 'davidhalter/jedi-vim'           " awesome python autocompletion with vim
Plugin 'klen/python-mode'               " vim python-mode. pylint, rope, pydoc, breakpoints from box
Plugin 'mitsuhiko/vim-jinja'            " jinja support for vim
Plugin 'mitsuhiko/vim-python-combined'  " combined python 2/3 for vim
Plugin 'hynek/vim-python-pep8-indent'   " pep8 indent
Plugin 'jmcantrell/vim-virtualenv'      " virtualenv support in vim

" --- rust ---
Plugin 'rust-lang/rust.vim'             " vim support for rust file detection and syntax highlighting
Plugin 'racer-rust/vim-racer'           " rust code completion in vim via racer

" ael
Plugin 'ael.vim'

call vundle#end() " required
filetype on
filetype plugin on
filetype plugin indent on

"=====================================================
" general settings
"=====================================================
" colorscheme Tomorrow-Night
" set guifont=consolas:h13
" set guifont=dejavu\ sans\ mono\ for\ powerline:h18
" Guifont Anonymous Pro:h11
" guifont dejavu sans mono:h10
" enable syntax colors
" in gui mode we go with fruity and monaco 13
" in cli mode myterm looks better (fruity is gui only)
syntax on
" syntax enable
set background=dark
" set background=light
colorscheme solarized
let g:solarized_termcolors=256
" if has("gui_running")
"     set macmeta
"     set lines=50 columns=125
" endif
" " special settings for vim
" if has("mac")
"     let macvim_hig_shift_movement = 1
" endif


set backspace=indent,eol,start
" this must happen before the syntax system is enabled
" let no_buffers_menu=1
set mousemodel=popup
" устанавливаем вертикальную линию
set colorcolumn=80
" переопределяем лидер кнопку
let mapleader=","
" отключаем перенос строк
set nowrap
" включить подсветку невидимых символов
set list 
set listchars=eol:↲,tab:--,trail:·,nbsp:·
" разбивать окно горизонтально снизу
set splitbelow
" разбивать окно вертикально справа
set splitright
" включаем подсветку выражения, которое ищется в тексте
set hlsearch
" отключение подсветки найденных выражений
set nohlsearch
" при поиске перескакивать на найденный текст в процессе набора строки (инкрементальный поиск)
set incsearch
" если искомое выражения содержит символы в верхнем регистре — ищет с учётом регистра, иначе — без учёта
set smartcase
" останавливать поиск при достижении конца файла
" set nowrapscan
" игнорировать регистр букв при поиске
set ignorecase
"-=swap и backup=-
" включаем создание swap файлов
set swapfile
" все swap файлы будут помещаться в заданную директорию (туда скидываются открытые буферы)
set dir=~/.vim/swap/
" включаем создание бекапов (резервные копии файлов с окончанием «~" создаваться не будут)
set backup
" если установлено set backup, то помещаются в этот каталог
set backupdir=~/.vim/backup/


" activate a permanent ruler and add line highlightng as well as numbers.
" also disable the sucking pydoc preview window for the omni completion
" and highlight current line and disable the blinking cursor.
set ruler
set completeopt-=preview
set gcr=a:blinkon0
if has("gui_running")
    set cursorline
endif
" set ttyfast

" tab sball
" set switchbuf=useopen

" use system clipboard
set clipboard=unnamedplus

" customize the wildmenu
set wildmenu
set wildignore+=*.dll,*.o,*.pyc,*.bak,*.exe,*.jpg,*.jpeg,*.png,*.gif,*$py.class,*.class,*/*.dsym/*,*.dylib
set wildmode=list:full

" don't bell and blink
set visualbell t_vb=    " turn off error beep/flash
set novisualbell        " turn off visual bell
" set enc=utf-8           " utf-8 default encoding
set ls=2                " always show status bar
set incsearch           " incremental search
set hlsearch            " highlighted search results
set nu                  " line numbers
set scrolloff=5         " keep some more lines for scope
set showmatch           " show matching brackets/parenthesis
set matchtime=0         " don't blink when matching

" swaps and backups
if has("win32") || has("win64")
    set dir=$tmp
    set backupdir=$tmp
else
    set dir=/tmp
    set backupdir=/tmp
endif

" hide some panels
"set guioptions-=m  " remove menu bar
set guioptions-=t   " remove toolbar
"set guioptions-=r  " remove right-hand scroll bar

" tab settings
set smarttab
set tabstop=8

" highlight characters past column 120
augroup vimrc_autocmds
    autocmd!
    autocmd Filetype ruby,python,javascript,c,cpp highlight excess ctermbg=darkgrey guibg=#c12a0f
    autocmd Filetype ruby,python,javascript,c,cpp match excess /\%80v.*/
    autocmd Filetype ruby,python,javascript,c,cpp set nowrap
augroup end

" snipmate settings
let g:snippets_dir = "~/.vim/vim-snippets/snippets"
" tagbar настройки
map <f4> :TagbarToggle<cr>
let g:tagbar_autofocus = 0 " автофокус на tagbar при открытии

" nerdtree настройки
" показать nerdtree на f3
map <f3> :NERDTreeToggle<cr>
"игноррируемые файлы с расширениями
let NERDTreeIgnore=['\~$', '\.pyc$', '\.pyo$', '\.class$', 'pip-log\.txt$', '\.o$']  


" tasklist настройки
map <f2> :TaskList<cr> 	   " отобразить список тасков на f2

" conqueterm
nnoremap <f5> :ConqueTermSplit ipython<cr> " run python-scripts at <f5>
nnoremap <f6> :exe "ConqueTermSplit ipython " . expand("%")<cr> " and debug-mode for <f6>
let g:conqueterm_startmessages = 0
let g:conqueterm_closeonend = 0

" jedi-vim
let g:jedi#show_call_signatures = 1 " show call signatures
let g:jedi#popup_on_dot = 1         " enable autocomplete on dot
let g:jedi#popup_select_first = 0   " disable first select from auto-complete

" syntastic
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_enable_signs = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_aggregate_errors = 1
noremap <f7> :w<cr>:syntasticcheck<cr>

" better :sign interface symbols
let g:syntastic_error_symbol = 'x'
let g:syntastic_style_error_symbol = 'x'
let g:syntastic_warning_symbol = 'x'
let g:syntastic_style_warning_symbol = 'x'

" vim-airline
" let g:airline_theme='powerlineish'

"=====================================================
" python-mode settings
"=====================================================
" python-mode
" activate rope
" keys:
" k show python docs
" <ctrl-space> rope autocomplete
" <ctrl-c>g rope goto definition
" <ctrl-c>d rope show documentation
" <ctrl-c>f rope find occurrences
" <leader>b set, unset breakpoint (g:pymode_breakpoint enabled)
" [[ jump on previous class or function (normal, visual, operator modes)
" ]] jump on next class or function (normal, visual, operator modes)
" [m jump on previous class or method (normal, visual, operator modes)
" ]m jump on next class or method (normal, visual, operator modes)
let g:pymode_rope = 0

" documentation
let g:pymode_doc = 0
let g:pymode_doc_key = 'k'
"linting
let g:pymode_lint = 1
let g:pymode_lint_checkers = ['pep8']
let g:pymode_lint_cwindow = 1
let g:pymode_lint_ignore="ew051,2501,w601,c0110,c0111"
let g:pymode_lint_write = 0

" support virtualenv
let g:pymode_virtualenv = 1

" enable breakpoints plugin
let g:pymode_breakpoint = 1
let g:pymode_breakpoint_key = '<leader>b'

" syntax highlighting
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all

" don't autofold code
let g:pymode_folding = 0

" get possibility to run python code
let g:pymode_run = 0

" other options
let g:pymode_options_colorcolumn = 0
if has("gui_running")
    let g:airline_powerline_fonts = 1
else
    let g:airline_powerline_fonts = 0
endif

"=====================================================
" user hotkeys
"=====================================================
" easier moving of code blocks
vnoremap < <gv " shift+> keys
vnoremap > >gv " shift+< keys

" backspace in visual mode deletes selection
vnoremap <bs> d

" " ctrl-z is undo
" noremap <c-z> u
" inoremap <c-z> <c-o>u
" 
" " ctrl-y is redo
" noremap <c-y> <c-r>
" inoremap <c-y> <c-o><c-r>
" 
" " ctrl-a is select all
" noremap <c-a> gggh<c-o>g
" inoremap <c-a> <c-o>gg<c-o>gh<c-o>g
" cnoremap <c-a> <c-c>gggh<c-o>g
" onoremap <c-a> <c-c>gggh<c-o>g
" snoremap <c-a> <c-c>gggh<c-o>g
" xnoremap <c-a> <c-c>ggvg
" 
" " ctrl-s is quicksave command
" noremap <c-s> :update<cr>
" vnoremap <c-s> <c-c>:update<cr>
" inoremap <c-s> <c-o>:update<cr>

" settings for buffers
map <c-q> :bd<cr>         " close current buffer
" noremap <c-right> :bn<cr> " move to next buffer
" noremap <c-left> :bp<cr>  " move to previous buffer

" easier split navigations
nnoremap <c-j> <c-w><c-j>
nnoremap <c-k> <c-w><c-k>
nnoremap <c-l> <c-w><c-l>
nnoremap <c-h> <c-w><c-h>

" easier change size for splitted windows
nnoremap <m-[> :vertical resize +5<cr>
nnoremap <m-]> :vertical resize -5<cr>

" activate autocomplete at <ctrl+space>
inoremap <c-space> <c-x><c-o>

" generate and insert uuid4 into code by <f12> key
nnoremap <f12> :call insertuuid4()<cr>

" python code check on pep8
" autocmd filetype python map <buffer> <leader>8 :pymodelint<cr>

"=====================================================
" languages support
"=====================================================
" --- c/c++/c# ---
autocmd Filetype c setlocal tabstop=4 softtabstop=4 shiftwidth=4 expandtab
autocmd Filetype cpp setlocal tabstop=4 softtabstop=4 shiftwidth=4 expandtab
autocmd Filetype cs setlocal tabstop=4 softtabstop=4 shiftwidth=4 expandtab
autocmd Filetype c setlocal commentstring=/*\ %s\ */
autocmd Filetype cpp,cs setlocal commentstring=//\ %s
let c_no_curly_error=1
let g:syntastic_cpp_include_dirs = ['include', '../include']
let g:syntastic_cpp_compiler = 'clang++'
let g:syntastic_c_include_dirs = ['include', '../include']
let g:syntastic_c_compiler = 'clang'

" --- clojure ---
autocmd Filetype clj setlocal tabstop=2 softtabstop=2 shiftwidth=2 expandtab

" --- css ---
autocmd Filetype css set omnifunc=csscomplete#completecss
autocmd Filetype css setlocal expandtab shiftwidth=4 tabstop=4 softtabstop=4
autocmd Filetype css setlocal commentstring=/*\ %s\ */

" --- erlang ---
autocmd Filetype erlang setlocal omnifunc=erlang_complete#complete

" --- javascript ---
autocmd Filetype javascript set omnifunc=javascriptcomplete#completejs
autocmd bufnewfile,bufread *.json setlocal ft=javascript
autocmd Filetype javascript setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
autocmd Filetype javascript setlocal commentstring=//\ %s
autocmd Filetype javascript let b:javascript_fold = 0
let javascript_enable_domhtmlcss=1
let g:syntastic_javascript_checkers = ['jshint']
let g:syntastic_javascript_jshint_args='--config ~/.vim/extern-cfg/jshint.json'

" --- html ---
autocmd Filetype html set omnifunc=htmlcomplete#completetags
autocmd Filetype html setlocal commentstring=<!--\ %s\ -->

" --- python ---
let python_highlight_all=1
let python_highlight_exceptions=0
let python_highlight_builtins=0
let python_slow_sync=1
autocmd Filetype python setlocal completeopt-=preview
autocmd Filetype python setlocal expandtab shiftwidth=4 tabstop=8
\ formatoptions+=croq softtabstop=4 smartindent
\ cinwords=if,elif,else,for,while,try,except,finally,def,class,with
autocmd Filetype pyrex setlocal expandtab shiftwidth=4 tabstop=8 softtabstop=4 smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class,with
let g:syntastic_python_checkers = ['flake8', 'python']
let g:syntastic_python_flake8_args='--ignore=e121,e128,e711,e301,e261,e241,e124,e126,e721
\ --max-line-length=80'

" --- rust ---
set hidden
let g:racer_cmd = "/users/savicvalera/racer/target/release/racer"
let $rust_src_path = "/users/savicvalera/rust/src"
autocmd bufread,bufnewfile *.rs set filetype=rust
autocmd Filetype rust setlocal expandtab shiftwidth=4 tabstop=8 softtabstop=4
autocmd Filetype rust setlocal commentstring=//\ %s

" --- vim ---
autocmd Filetype vim setlocal expandtab shiftwidth=2 tabstop=8 softtabstop=2

" --- template language support (sgml / xml too) ---
autocmd Filetype xml,html,htmljinja,htmldjango setlocal expandtab shiftwidth=2 tabstop=2 softtabstop=2
autocmd Filetype html,htmljinja,htmldjango imap <buffer> <c-e> <plug>sparkupexecute
autocmd Filetype html,htmljinja,htmldjango imap <buffer> <c-l> <plug>sparkupnext
autocmd Filetype htmljinja setlocal commentstring={#\ %s\ #}
let html_no_rendering=1
let g:syntastic_html_checkers = []

"=====================================================
" user functions
"=====================================================
" small helper that inserts a random uuid4
" ----------------------------------------
" function! insertuuid4()
" python << endpython
" if 1:
"     import uuid, vim
"     s = str(uuid.uuid4())
"     cpos = vim.current.window.cursor
"     cline = vim.current.line
"     vim.current.line = cline[:cpos[1] + 1] + s + cline[cpos[1] + 1:]
"     vim.current.window.cursor = (cpos[0], cpos[1] + len(s))
" endpython
" endfunction
